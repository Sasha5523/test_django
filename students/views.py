from django.shortcuts import render
from django.http import HttpResponse
from students.utils import parse_length, gen_password
# Create your views here.
def hello(request):
    return HttpResponse('Hello from Django!')


def get_random(request):
    try:
        length = parse_length(request, 10)
        # chars = parse_chars(request, 0)
    except Exception as ex:
        return HttpResponse(str(ex), status_code=400)

    result = gen_password(length)

    return HttpResponse(result)

import datetime

from django.db import models

# Create your models here.
class Student(models.Model):
    first_name = models.CharField(max_length=64, null=False)
    last_name = models.CharField(max_length=84, null=False)
    birthdate = models.DateTimeField(null=True, default=datetime.date.today)



class Group(models.Model):
    group_name = models.CharField(max_length=8, null=False)
    course = models.CharField(max_length=10, null=False)
    amount_student = models.CharField(max_length=25, null=True)


class Teachers(models.Model):
    first_name = models.CharField(max_length=64, null=False)
    last_name = models.CharField(max_length=84, null=False)
    Specialization = models.CharField(max_lenght=20, null=False)

from django.apps import AppConfig


class SaudentsConfig(AppConfig):
    name = 'saudents'
